# TechLit Artix Linux

This code is installed on all TechLit systems. It is used to bootstrap and update all system components.

## Prerequisites

The only requirement for this code is that it runs on an artix linux machine; any TechLit machine (v18+) will do.

If you'd like to use another system, and you set up virtualization, please share instructions here.

## Getting Started

First, clone the code onto your artix machine (if you're on a TechLit machine, then you probably already have
the code at `/opt/admin`).

After cloning, use the `tl-comp-sync` script to set up the components that you're interested in.

```
git clone git@gitlab.com:techlit-africa/admin /opt/admin
cd /opt/admin/admin
./bin/tl-comp-sync
```

## Command Documentation

To avoid extra work and out-of-date docs,
run the command without any arguments to print the documentation.

For a comprehensive command documentation check out [TechLit Desktop help page](https://help.techlitafrica.org/docs/desktop/admin).
