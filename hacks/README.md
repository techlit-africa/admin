# Hacks

"Hacks" are workarounds for various hardware categories on TechLit computers.

# File Structure

## `/<hw>`
Each directory here is a category of hardware with a collection of hacks inside.
Each hack (a category sub-folder, like "mesa-amber") has four files.

All of the scripts inside expect to be run as `admin` or `root` with `$ROOT` and `$BTRFS` defined and
the full TechLit admin lib loaded from `$ROOT/lib/lib.bash`.

## `./<hw>/more`
A short phrase explaining what the hack is. Not executable, just text.

## `/<hw>/check`
Tests the system to see if the hack is a good idea. Returns 0 if yes, 1 if no.
This runs when `"defaults"` is enabled for this category (see "Enabling Hacks" below for more info).

## `/<hw>/hack`
Hacks the system with the workaround.
This only runs when `"defaults"` is enabled and `check` returns true, or when this hack is manually selected.

# Enabling Hacks

## Configuration Tool

The easiest way to manually enable / disable one or more hacks is to use the `tl-sys-configure` script.

## By Hand

Each category has a collection of hacks that can be disabled or enabled explicitly in `/srv/secure/config.json`.
By default (and to use default checks and behavior) `config.json` will look like this:

```
{
  "role": "client",
  "hostname": "tl-0000",
  "wifi_name": "TechLit Africa",
  "wifi_password": "empowermogotio",
  "wifi_hacks": "defaults",
  "gpu_hacks": "defaults",
}
```

With one or more hacks enabled, it will look like this:

```
{
  "role": "client",
  "hostname": "tl-0000",
  "wifi_name": "TechLit Africa",
  "wifi_password": "empowermogotio",
  "wifi_hacks": "b43,hardcode-mac",
  "gpu_hacks": "mesa-amber,nomodeset",
}
```
