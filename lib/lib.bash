#!/hint/bash
# vim:et:ts=2:sts=2:sw=2
# DO NOT LOAD INTERACTIVELY
# shellcheck disable=SC2155
if [[ $- =~ i ]]; then
  echo "${BASH_SOURCE[0]} is not meant for interactive use" >&2
  echo "Refusing to continue" >&2
  return 1
fi

#
# Load configuration first
#
source "$(realpath "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/cfg.bash"

#
# Shell options (see bash manual for more info)
#
# -e          # Exit on first error
# -E          # ERR traps called from functions too
# -T          # DEBUG and RETURN traps called from functions too
# -o pipefail # Debug
#
set -eETo pipefail

#
# When using getopts, print errors
#
OPTERR=1

#
# Color shortcuts
#
# r=red, g=green, y=yellow, c=cyan, x=reset
#
# Print the variable version to change to a color
#
# Call the function version to print in a color,
#   then change back to yellow
#
TERM="${TERM:-xterm}"
r="$(echo -e '\e[1;31m')"
r() { echo -e "$r$*$y"; }
g="$(echo -e '\e[1;32m')"
g() { echo -e "$g$*$y"; }
y="$(echo -e '\e[1;33m')"
y() { echo -e "$y$*$y"; }
c="$(echo -e '\e[1;36m')"
c() { echo -e "$c$*$y"; }
x="$(echo -e '\e[0m')"

#
# Logging helpers
#
# say prints a colorful, indented comment
say() { echo -e "$*" | sed -e "s/^/$y# => /g" | sed -e "s/$/$x/g" >&2; }
# err prints a colorful, indented error
err() { echo -e "$*" | sed -e "s/^/$r# !! /g" | sed -e "s/$/$x/g" >&2; }
# run prints a colorful, indented command, then runs it
run() {
  echo -e "$y# $c$ $g$*$x" >&2
  "$@"
}

#
# Cleanup helpers
#
# Run a cleanup function before exiting
trap 'cleanup; cleanup() { :; }' EXIT ERR HUP INT QUIT TERM
# Redefine cleanup wherever to override exit behavior
cleanup() { :; }

#
# Device identifiers
#
is-t2-chip() {
  if grep -Eiq 'iMacPro1|MacBookPro(13|14|15|16)|MacBookAir(8|9)|Macmini(8|9)' \
    /sys/class/dmi/id/product_name; then
    return 0
  else
    return 1
  fi
}
export -f is-t2-chip

set-partitions() {
  local disk="$1"
  local p

  [[ "$disk" =~ mmc ]] || [[ "$disk" =~ nvm ]] && p=p || p=

  if is-t2-chip; then
    export ROOT_PART="${disk}${p}2" EFI_PART="${disk}${p}1"
  else
    export ROOT_PART="${disk}${p}4" EFI_PART="${disk}${p}2"
  fi
}

# Checks if a binary is already signed using our keys
# Needs KEYS var setup
# USAGE: is-signed binary
is-signed() {
  if ! command sbverify --version &>/dev/null; then
    err "$(c sbsigntools) not installed. Recovery image outdated? $(g v5.3.0) or later required."
  elif ! sbverify --cert "$KEYS"/MOK.crt "$1" &>/dev/null; then
    return 1
  fi
  return 0
}

#
# Chroot helpers
#
# NOTE: Before calling the functions, you need to set:
#       - PREFIX # the target root directory

# USAGE: tl-chroot "root:wheel" "x && y 2>&1"
tl-chroot() { sudo unshare --pid --fork chroot --userspec "$1" "${PREFIX:?}" "/bin/bash" -c "$2"; }
# USAGE: root-chroot "x && y 2>&1"
root-chroot() { tl-chroot "root:root" "$1"; }
# USAGE: admin-chroot ". ~/.bashrc"
admin-chroot() { tl-chroot "admin:admin" "$1"; }

# USAGE: tl-chroot-mount
tl-chroot-mount() {
  # Required mounts for chroot
  for dir in sys dev proc; do
    run sudo mkdir -p "$PREFIX/$dir"
    run sudo mount --rbind "/$dir" "$PREFIX/$dir"
  done

  # Simplest possible resolvconf
  run sudo mkdir -p "$PREFIX/etc"
  run root-write "$PREFIX/etc/resolv.conf" "nameserver 8.8.8.8"
}

# USAGE: tl-chroot-unmount
tl-chroot-unmount() {
  for dir in sys dev proc; do
    run sudo umount -qR "$PREFIX/$dir" || :
  done
}

#
# Filesystem helpers
#
# These are useful in combination when logging with the run helper
#
# Returns the UUID of the specified partition.
get_uuid() {
  local uuid="$(lsblk "$1" -no UUID)"
  if [[ -z "$uuid" ]]; then
    uuid="$(sudo lsblk "$1" -no UUID)"
    if [[ -z "$uuid" ]]; then
      uuid="$(blkid "$1" --output value -s UUID)"
      if [[ -z "$uuid" ]]; then
        uuid="$(sudo blkid "$1" --output value -s UUID)"
      fi
    fi
  fi
  echo "$uuid"
}

# USAGE: run write "rc.conf" "x = y"
write() { echo "$2" >"$1"; }

# USAGE: run append "rc.conf" "x = y"
append() { echo "$2" >>"$1"; }

# USAGE: run root-write "rc.conf" x = y"
root-write() {
  local t="$(mktemp)"
  write "$t" "$2" && sudo cp "$t" "$1"
}

# USAGE: run root-append "rc.conf" x = y"
root-append() {
  local t="$(mktemp)"
  cp "$1" "$t"
  append "$t" "$2" && sudo cp "$t" "$1"
}

#
# Hack helpers
#
# NOTE: You can optionally override these variables:
#       - BTRFS # the BTRFS root path (default /var/btrfs)
#       - PREFIX # the root for chroot commands (default $BTRFS/@root)
#       - CACHE # the alternative package cache location (default /usr/local/src)
#

# USAGE: tl-alt-pacman -U mesa-amber
# USAGE: tl-alt-pacman -Rs xf86-video-intel
tl-alt-pacman() {
  BTRFS="${BTRFS:-"/var/btrfs"}"
  PREFIX="${PREFIX:-"$BTRFS/@root"}"
  CACHE="${CACHE:-"/usr/local/src"}"

  run sed -i -e 's/^CheckSpace/# CheckSpace/' "$BTRFS/@root/etc/pacman.conf"
  # We pipe the yesses here because sometimes in a chroot world GPG doesn't trust us
  run root-chroot "yes | pacman --cachedir=$CACHE $*"
  run sed -i -e 's/# CheckSpace/CheckSpace/' "$BTRFS/@root/etc/pacman.conf"
}

#
# Install helpers
#
# NOTE: Before calling the functions, you need to set:
#       - SRC  # the source directory
#       - DEST # the destination directory

# USAGE: tl-install rc.conf 0 0 640 rc.conf
tl-install() {
  install -o "$2" -g "$3" -m "$4" -vD "$SRC/$1" "$DEST/$5"
}

# USAGE: tl-root-install rc.conf 0 0 640 rc.conf
tl-root-install() {
  sudo install -o "$2" -g "$3" -m "$4" -vD "$SRC/$1" "$DEST/$5"
}

# USAGE: tl-install-dir conf.d 0 0 640 conf.d
tl-install-dir() {
  [ -d "$DEST/$5" ] && rm -rf "${DEST:?}/$5"
  mkdir -p "$DEST/$5"

  (
    cd "$SRC/$1" || false
    find . -type f -exec install -o"$2" -g"$3" -m"$4" -vD "{}" "$DEST/$5/{}" \;
  )
}

# USAGE: tl-install-svc sva svb svc
tl-install-svc() {
  for svc in "$@"; do
    ln -svf "/etc/sv/$svc" "$DEST/etc/runit/runsvdir/default"
  done
}

#
# Bootstrap helpers
#
# NOTE: Before calling the functions, you need to set:
#       - PREFIX # the root of the system to be bootstrapped
#       - LOCALE # the initial locale for configuring packages
#
#       $PREFIX/srv/void-mklive is used to cache XBPS packages
#       $PREFIX/srv/techlit-void is used to install roles

# USAGE: tl-bootstrap-roles /srv/techlit-void rolea roleb...
tl-bootstrap-roles() {
  say "Installing roles:"
  for role in "$@"; do
    say "  $(c "$role")"
    run "$ROOT/roles/$role/install" "$PREFIX"
  done
}

# USAGE: tl-bootstrap-kernel
tl-bootstrap-kernel() {
  vmlinuz="$(find "$PREFIX/boot/" -name "vmlinuz*" | sort | tail -n1)"
  initrd="$(find "$PREFIX/boot/" -name "initramfs*" | sort | tail -n1)"

  say "Linking latest kernel and initramfs"
  run ln -srf "$vmlinuz" "$PREFIX/boot/vmlinuz"
  run ln -srf "$initrd" "$PREFIX/boot/initrd"
}

#
# User helpers
#
assert-is-user() {
  if [ -n "$SKIP_USER_CHECK" ]; then
    return
  fi

  if [ "$(id -u)" = 0 ]; then
    err "This script is meant to be run as a regular user. Refusing to continue"
    say "If you know what you're doing, you can set $(c SKIP_USER_CHECK)"
    exit 1
  fi
}

assert-is-admin() {
  if [ -n "$SKIP_USER_CHECK" ]; then
    return
  fi

  if [ "$(id -u)" != "$ADMIN_UID" ]; then
    err "This script is meant to be run as a the $(c admin) user. Refusing to continue"
    say "If you know what you're doing, you can set $(c SKIP_USER_CHECK)"
    exit 1
  fi
}

assert-is-root() {
  if [ -n "$SKIP_USER_CHECK" ]; then
    return
  fi

  if ! [ "$(id -u)" = 0 ]; then
    err "This script is meant to be run as ${c}root${r}. Refusing to continue"
    say "If you know what you're doing, you can set $(c SKIP_USER_CHECK)"
    exit 1
  fi
}

#
# Repo helpers
#
assert-fetched() {
  for comp in "${@}"; do
    if ! [ -d "/srv/$comp" ] && ! [ -f "/srv/.tl-repo/$comp.local" ]; then
      err "$c$comp$r is required. Refusing to continue"
      say "HINT: Run $(g tl-repo-fetch) $(c "$comp")"
      exit 1
    fi
  done
}
