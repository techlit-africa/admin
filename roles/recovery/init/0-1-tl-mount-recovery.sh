#!/bin/sh

# Mount a persistent partition before initialization
if ! grep -q "/srv" "/etc/mtab" >"/dev/null"; then
  sdx="$(grep "/run/initramfs/live" "/etc/mtab" | sed -e 's/p\?. .*//')"

  if grep -q "recovery" "/proc/cmdline"; then
    # If we're in recovery mode, mount a btrfs @srv subvolume
    mount "${sdx}4" -o "subvol=@srv" "/srv"
  else
    # If we're in installer mode, mount the whole 4th partition
    mkdir -p "/srv"
    mount "${sdx}4" "/srv"
  fi
fi
