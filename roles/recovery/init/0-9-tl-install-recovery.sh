#!/bin/sh

for role in global hardware recovery; do
  [ -x "/srv/techlit-void/roles/$role/install" ] \
    && "/srv/techlit-void/roles/$role/install" "/"
done
