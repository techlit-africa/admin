#!/bin/sh

# Link network state directories
if [ -d "/srv/secure/net/lib" ]; then
  rm -rf "/var/lib/NetworkManager"
  ln -sf "/srv/secure/net/lib" "/var/lib/NetworkManager"
fi

if [ -d "/srv/secure/net/system-connections" ]; then
  rm -rf "/etc/NetworkManager/system-connections"
  ln -sf "/srv/secure/net/system-connections" "/etc/NetworkManager/system-connections"
fi
