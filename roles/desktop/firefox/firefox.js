// This is the Debian specific preferences file for Mozilla Firefox
// You can make any change in here, it is the purpose of this file.
// You can, with this file and all files present in the
// /etc/firefox/pref directory, override any preference that is
// present in /usr/lib/firefox/defaults/pref directory.
// While your changes will be kept on upgrade if you modify files in
// /etc/firefox/pref, please note that they won't be kept if you
// do them in /usr/lib/firefox/defaults/pref.

pref("extensions.update.enabled", false);

// Use LANG environment variable to choose locale
pref("intl.locale.matchOS", true);
pref("intl.locale.requested", "");

// Disable default browser checking.
pref("browser.shell.checkDefaultBrowser", false);

// Prevent EULA dialog to popup on first run
pref("browser.EULA.override", true);

// Set the UserAgent
pref("general.useragent.vendor", "TechLit Africa");

// Default search engine
pref("browser.search.searchEnginesURL", "https://google.com");

// Activate the backspace key for browsing back
pref("browser.backspace_action", 0);

// Ignore Mozilla release notes startup pages
pref("browser.startup.homepage_override.mstone", "ignore");

// Don't interrupt user workflow
pref('datareporting.policy.dataSubmissionPolicyBypassNotification', true);
pref('browser.slowStartup.notificationDisabled', true);
pref('browser.disableResetPrompt', true);
pref("browser.rights.3.shown", true);
pref("toolkit.telemetry.prompted", 2);
pref("toolkit.telemetry.rejected", true);

// identify default locale to use if no /usr/lib/firefox-addons/searchplugins/LOCALE
// exists for the current used LOCALE
pref("distribution.searchplugins.defaultLocale", "en-US");

// Enable the NetworkManager integration
//pref("network.manage-offline-status", true);

// Don't disable our bundled extensions in the application directory
pref("extensions.autoDisableScopes", 0);
pref("extensions.shownSelectionUI", true);

// Map to hyphenation patterns from openoffice.org-hyphenation and openoffice.org-dictionaries
pref("intl.hyphenation-alias.en", "en-us");
pref("intl.hyphenation-alias.en-*", "en-us");

// Disable Telemetry
pref("toolkit.telemetry.unified", false);
pref("toolkit.telemetry.archive.enabled", false);
pref("toolkit.telemetry.enabled", false);
pref("toolkit.telemetry.unifiedIsOptIn", false);
pref("toolkit.telemetry.server", "data:,");
pref("toolkit.telemetry.newProfilePing.enabled", false);
pref("toolkit.telemetry.shutdownPingSender.enabled", false);
pref("toolkit.telemetry.updatePing.enabled", false);
pref("toolkit.telemetry.bhrPing.enabled", false);
pref("toolkit.telemetry.firstShutdownPing.enabled", false);
pref("toolkit.telemetry.coverage.opt-out", true);
pref("toolkit.coverage.opt-out", true);
pref("toolkit.coverage.endpoint.base", "");
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("datareporting.healthreport.uploadEnabled", false);
pref("app.shield.optoutstudies.enabled", false);
pref("browser.discovery.enabled", false);
pref("breakpad.reportURL", "");
pref("browser.tabs.crashReporting.sendReport", false);
pref("browser.crashReports.unsubmittedCheck.enabled", false);
pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false);

// Disable welcome pages
pref("startup.homepage_welcome_url", "http://localhost");
pref("startup.homepage_welcome_url.additional", "");
pref("startup.homepage_override_url", ""); // what's new page afer update
pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
pref("browser.newtabpage.activity-stream.feeds.snippets", false);

// Remove sync account from toolbar
pref("identity.fxaccounts.toolbar.enabled", false); // Firefox Accounts & Sync [FF60+] [RESTART]
pref("identity.fxaccounts.toolbar.accessed", true); // Firefox Accounts & Sync [FF60+] [RESTART]

// Disable modern search config
pref("browser.search.modernConfig", false);
