flush ruleset

table inet base {
  set lan4 {
    type ipv4_addr
    flags interval
    elements = { 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16, 169.254.0.0/16 }
  }

  set lan6 {
    type ipv6_addr
    flags interval
    elements = { fd00::/8, fe80::/10 }
  }

  chain any_forwarding {
    policy drop;
    type filter hook forward priority filter;
  }

  chain any_output {
    policy accept;
    type filter hook output priority filter;
  }

  chain any_input {
    type filter hook input priority filter; policy drop;

    iif lo accept comment "Accept any localhost traffic"
    ct state invalid drop comment "Drop invalid connections"
    ct state established,related accept comment "Accept traffic originated from us"

    meta l4proto icmp accept comment "Accept IPv4 ICMP"
    meta l4proto ipv6-icmp accept comment "Accept IPv6 ICMP"
    ip protocol igmp accept comment "Accept IGMP"

    ip6 saddr @lan6 jump lan_input comment "Connections from private IP address ranges"
    ip saddr @lan4 jump lan_input comment "Connections from private IP address ranges"
  }

  chain lan_input {
    udp dport mdns ip daddr 224.0.0.251 accept comment "Accept mDNS (IPv4 addr)"
    udp dport mdns ip6 daddr ff02::fb accept comment "Accept mDNS (IPv6 addr)"

    tcp dport ssh accept comment "Accept SSH on port 22"

    tcp dport { http, https } accept comment "Accept HTTP (ports 80, 443)"
  }
}
