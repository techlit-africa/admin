#!/bin/sh

# Start plash screen before anything else
if [ -x "/bin/tl-boot-splash" ] && grep -q "splash" "/proc/cmdline"; then
  "/bin/tl-boot-splash"
fi
