#!/bin/sh

# Link host's fstab before initialization
if [ -r "/srv/secure/fstab" ]; then
  ln -sf "/srv/secure/fstab" "/etc/fstab"
fi
