#!/usr/bin/env bash

# Get install path
DEST="${1:?'Where to install?'}"

# Get absolute path of this directory (especially when symlinked)
SRC="$(realpath "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")"

# Load accompanying bash library
source "$(dirname "$(dirname "$SRC")")/lib/lib.bash"

say "Disabling all existing services"
run rm -rf "$DEST/etc/runit/runsvdir/default"
run mkdir -p "$DEST/etc/runit/runsvdir/default"

say "Installing $(basename "$SRC") services"
tl-install-svc \
  udevd socklog-unix nanoklogd

say "Installing $(basename "$SRC") files"
tl-install "init/0-9-tl-rc-conf.sh"    0 0 644 "etc/runit/core-services/0-9-tl-rc-conf.sh"
tl-install "init/04-tl-hostname.sh"    0 0 644 "etc/runit/core-services/04-tl-hostname.sh"
tl-install "init/92-tl-ssh-state.sh"   0 0 644 "etc/runit/core-services/92-tl-ssh-state.sh"
tl-install "init/93-tl-admin-state.sh" 0 0 644 "etc/runit/core-services/93-tl-admin-state.sh"

tl-install-dir "xbps-keys" 0 0 644 "var/db/xbps/keys/"
tl-install "xbps/12-techlit-nonfree.conf" 0 0 644 "etc/xbps.d/12-techlit-nonfree.conf"

tl-install "sshd_config"       0 0 640 "etc/ssh/sshd_config"
tl-install "sudoers"           0 0 640 "etc/sudoers"
tl-install "lighttpd.conf"     0 0 640 "etc/lighttpd/lighttpd.conf"

tl-install "admin/bash_profile" "$ADMIN_UID" "$ADMIN_GID" 640 "home/admin/.bash_profile"
tl-install "admin/bash_colors"  "$ADMIN_UID" "$ADMIN_GID" 640 "home/admin/.bash_colors"
tl-install "admin/bash_aux"     "$ADMIN_UID" "$ADMIN_GID" 640 "home/admin/.bash_aux"
tl-install "admin/htoprc"       "$ADMIN_UID" "$ADMIN_GID" 640 "home/admin/.htoprc"
tl-install "admin/tmux.conf"    "$ADMIN_UID" "$ADMIN_GID" 640 "home/admin/.tmux.conf"
tl-install "admin/vimrc"        "$ADMIN_UID" "$ADMIN_GID" 640 "home/admin/.vimrc"
