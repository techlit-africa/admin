#!/usr/bin/env bash
# vim:et:ts=2:sts=2:sw=2

# Get absolute repository root (especially when symlinked)
ROOT="$(realpath "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")")"

# Load accompanying bash library
source "$ROOT/lib/lib.bash"

# Exit now if run as root user
assert-is-root

# Path to USB disk
DISK="$1"
if [ -z "$DISK" ] || [ -n "$HELP" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  say "
  USAGE:
    $(g "${BASH_SOURCE##*/}")  $(c /dev/sdx)

  DESCRIPTION:
    $(g "${BASH_SOURCE##*/}") installs and configures bootloaders for the
    TechLit installer USB on $(c /dev/sdx).
  "
  exit 1
fi

# Mountpoint to use
PREFIX="${PREFIX:-"$(mktemp -d)"}"

# partition infix
[[ "$DISK" =~ mmc ]] || [[ "$DISK" =~ nvm ]] && p=p || p=

say "Mounting system (and un-mounting on errors or exit)"
{
  cleanup() {
    say "Un-mounting system"
    run umount -qR "$PREFIX"
  }
  run mount "$DISK$p"2 "$PREFIX"
}

GRUB_MODULES="all_video boot btrfs cat chain configfile echo efifwsetup efinet \
ext2 fat font gettext gfxmenu gfxterm gfxterm_background gzio halt help hfsplus \
iso9660 jpeg keystatus loadenv loopback linux ls lsefi lsefimmap lsefisystab \
lssal memdisk minicmd normal ntfs part_apple part_msdos part_gpt password_pbkdf2 \
png probe reboot regexp search search_fs_uuid search_fs_file search_label sleep \
smbios squash4 test true video xfs zfs zfscrypt zfsinfo cpuid play tpm cryptodisk \
gcry_arcfour gcry_blowfish gcry_camellia gcry_cast5 gcry_crc gcry_des gcry_dsa \
gcry_idea gcry_md4 gcry_md5 gcry_rfc2268 gcry_rijndael gcry_rmd160 gcry_rsa \
gcry_seed gcry_serpent gcry_sha1 gcry_sha256 gcry_sha512 gcry_tiger gcry_twofish \
gcry_whirlpool luks lvm mdraid09 mdraid1x raid5rec raid6rec keylayouts usbms usb_keyboard udf ext2 reiserfs"

SBAT="$ROOT"/roles/global/sbat.csv

install-grub() {
  local arch="$1"
  local disk="$2"
  [[ "$arch" =~ efi ]] && SECURE_ARGS=(--disable-shim-lock --modules="$GRUB_MODULES" --sbat "$SBAT")

  say "Installing $(c "$arch") bootloader"
  run grub-install \
    --force --recheck --removable --skip-fs-probe \
    "${SECURE_ARGS[@]}" \
    --target="$arch" \
    --efi-directory="$PREFIX" $NONVRAM \
    --boot-directory="$PREFIX" \
    "$disk"
}

for arch in i386-pc i386-efi x86_64-efi; do
  [ -d "$PREFIX"/grub/$arch ] && continue
  install-grub "$arch" "$DISK"
done

say "Configuring bootloaders"
{
  iso_uuid=$(lsblk "$DISK$p"3 -no UUID)

  run write "$PREFIX/grub/grub.cfg" "
insmod font
insmod keylayouts
insmod part_gpt

set default=0
set timeout=0

menuentry 'TechLit Installer'{
	search --no-floppy --fs-uuid --set=root $iso_uuid
	loopback loop $iso_uuid

  linux (loop)/boot/vmlinuz-linux
	initrd (loop)/boot/initramfs_x86_64.img
	configfile /boot/grub/grub.cfg
}
"
}

say "Setting up Secure boot"
{
  EFI="$PREFIX"/EFI/BOOT
  KEYS="$ROOT"/roles/global/secure-keys
  SHIM="$ROOT"/roles/global/shim

  run mv "$EFI"/BOOTX64.efi "$EFI"/grubx64.efi
  run cp "$SHIM"/shimx64.efi "$EFI"/BOOTX64.efi
  run cp "$SHIM"/mmx64.efi "$EFI"/

  say "Signing GRUB..."
  if ! is-signed "$EFI"/grubx64.efi; then
    run sbsign --key "$KEYS"/MOK.key --cert "$KEYS"/MOK.crt --output \
      "$EFI"/grubx64.efi "$EFI"/grubx64.efi
  fi

  run cp "$KEYS"/MOK.cer "$EFI/ENROLL_THIS.cer"
}

# Unmount system and quiet exit hook
# shellcheck disable=SC2218
cleanup
cleanup() { :; }
say "Done."
