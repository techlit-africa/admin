#!/usr/bin/env ruby
# vim:et:ts=2:sts=2:sw=2

STATDIR='/srv/secure/hosts'

if ARGV.length.zero? || ARGV[0] == '--help' || ARGV[0] == '-h'
  puts <<~EOF
    Usage:
      tl-stat <field1> [<field2> ...]

    Client Fields:
  EOF

  File.readlines("#{STATDIR}/server").each do |stat|
    next if stat.chomp.empty?

    puts "  " + stat.split[0].split('.')[1..].join('.')
  end

  puts <<~EOF
    Server Fields
      net.status
      net.ping
  EOF

  exit
end

fields = ARGV

table = {}

fields_max = {}

Dir.children(STATDIR).each do |host|
  table[host] = {}

  fields.each do |field, row|
    val = case field
    when /ping/
      File.read("/srv/secure/ping/#{host}").chomp.to_i.to_s + 'ms' rescue nil
    when /status/
      File.exist?("/srv/secure/ping/#{host}") ? 'online' : 'offline'
    else
      ''
    end

    table[host][field] = val

    if val && (fields_max[field].nil? || fields_max[field] < val.length)
      fields_max[field] = val.length
    end
  end

  File.readlines("#{STATDIR}/#{host}").each do |stat|
    next if stat.chomp.empty?

    key, *val_parts = stat.chomp.split(' ')
    val = val_parts.join(' ')

    fields.each do |field|
      if key.include?(field)
        table[host][field] = val

        if val && fields_max[field] < val.length
          fields_max[field] = val.length
        end
      end
    end
  end
end

table.keys.sort.each do |host|
  row = table[host]

  fields.each do |field|
    print row[field].to_s.ljust(fields_max[field].to_i + 2)
  end

  puts
end
